// Import libray
import React from 'react';
import { TouchableWithoutFeedback, Keyboard, View, AppRegistry } from 'react-native';
import NewCard from './src/components/NewCardView/NewCard';
import NewAddress from './src/components/NewAddressView/NewAddress';
import NewCEP from './src/components/NewAddressView/NewCEP';
import AgendaProfessor from './src/components/AgendaProfessor/AgendaProfessor';
import DiscountContainer from './src/components/DiscountView/DiscountContainer';

// Create a component

const App = () => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
        <NewCard />
      </View>
    </TouchableWithoutFeedback>
);

const DiscountView = () => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
        <DiscountContainer />
      </View>
    </TouchableWithoutFeedback>
);

// Render it to the device
AppRegistry.registerComponent('albums', () => App);
AppRegistry.registerComponent('DiscountView', () => DiscountView);
AppRegistry.registerComponent('NewAddress', () => NewAddress);
AppRegistry.registerComponent('Schedule', () => AgendaProfessor);
AppRegistry.registerComponent('NewCEP', () => NewCEP);
